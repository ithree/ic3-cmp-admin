class SavedReport < ActiveHash::Base
	attr_reader :args

	self.data = (SavedReports::Base.new().list_report_name)

	def self.column_names
		["id", "name"]
	end

	def self.quoted_table_name
		"\"miq_report_results\""
	end

	def self.connection

	end

	def initialize(args = nil)
		@args = args
	end

	def datas
		MiqReportResult.where(args[:name], created_on: args[:date])
	end

	def header
		datas.last.headers
	end

	def contents
	end

	def list_report_name
		arr = MiqReportResult.pluck(:id ,:name).uniq
		result = []
		arr.each do |name|
			result << {id: name[0], name: name[1]}
		end
		result
	end

end
