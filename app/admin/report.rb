ActiveAdmin.register MiqReportResult, as: "Saved Report" do
	filter :created_on
	filter :name

	index do
		column "Report Name", sortable: :name do |get| 
			link_to get.name, admin_saved_report_path(get) 
		end
		column "Queued At", :created_on
		column "Run At", :last_run_on
		column "Source", :report_source
		column "User Name", :userid
		column "Group", :miq_group
		column :status
	end

	show do |data|
		columns do
			attributes_table do
				data.report.headers.each do |head|
					row head do
					 head
					end
				end
			end
		end
	end
end
