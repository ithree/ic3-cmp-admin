module SavedReports
	class Base
		attr_reader :args
		attr_reader :result

		def initialize(args = nil)
			@args = args
		end
		
		def datas
			MiqReportResult.where(name: @args[:name], created_on: @args[:date])
		end	
		
		def header
			datas.last.headers
		end

		def contents
		end 
		
		def list_report_name
			arr = MiqReportResult.pluck(:id, :name).uniq
			result = []
			arr.each do |value|
				result << {id: value[0] , name: value[1]}
			end
			result
		end
	end
end
